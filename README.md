# pyFAI

[![pipeline status](https://gitlab.esrf.fr/silx/bob/pyfai/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/pyfai/pipelines)

Build binary packages for [pyFAI](https://github.com/silx-kit/pyFAI): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/pyfai)
